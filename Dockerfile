############################################################
# Dockerfile to build CGE Pipeline
# # Define app's environment with a Dockerfile so it can be reproduced anywhere:
############################################################

# Set base image to Python Anaconda
FROM continuumio/anaconda

# File Author / Maintainer
MAINTAINER Jose Luis Bellod Cisneros

# Update the repository sources list
RUN apt-get install debian-archive-keyring
RUN apt-key update

RUN apt-get update && apt-get install -y \
    aufs-tools \
    automake \
    btrfs-tools \
    build-essential \
    ca-certificates \
    curl \
    debian-archive-keyring \
    dpkg-sig \
    emacs \
    git \
    iptables \
    libapparmor-dev \
    libcap-dev \
    libmysqlclient-dev \
    libsqlite3-dev \
    lxc \
    mercurial \
    openssh-server \
    parallel \
    perl \
    reprepro
    

# Install services from Github (testing repositories)
# TODO Install deployment versions

RUN mkdir /root/.ssh/
RUN ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts
RUN ssh-keyscan cpanmin.us >> /root/.ssh/known_hosts
RUN echo "    IdentityFile ~/.ssh/id_rsa" >> /etc/ssh/ssh_config

RUN chmod 777 -R /tmp && chmod o+t -R /tmp

RUN mkdir /usr/src/cgepipeline
ADD . /usr/src/cgepipeline
WORKDIR /usr/src/cgepipeline

# INSTALLING SPADES-3.6.0
RUN wget \
    http://spades.bioinf.spbau.ru/release3.6.0/SPAdes-3.6.0-Linux.tar.gz
RUN tar -xzf SPAdes-3.6.0-Linux.tar.gz
ENV PATH $PATH:/usr/src/cgepipeline/SPAdes-3.6.0-Linux/bin

# INSTALLING BLAST-2.2.26
RUN curl \
  ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.2.26/ncbi-blast-2.2.26+-x64-linux.tar.gz \
  -o blast.tar.gz
RUN tar -zxvf blast.tar.gz

RUN echo 'export PATH=$PATH:/usr/src/cgepipeline/-2.2.26/bin' >> /root/.bash_profile
ENV PATH $PATH:/usr/src/cgepipeline/blast-2.2.26/bin

# CGE Tools
#################################################
RUN curl -L https://cpanmin.us -k | perl - App::cpanminus
# RUN cpan App::perlbrew
RUN cpanm Try::Tiny::Retry
RUN cpanm BioPerl --force
RUN cpanm Data::Dumper
RUN cpanm Getopt::Long
RUN cpanm File::Temp

# RUN perlbrew init

ENV PATH $PATH:/usr/src/cgepipeline/services/resfinder
ENV PATH $PATH:/usr/src/cgepipeline/services/mlst
ENV PATH $PATH:/usr/src/cgepipeline/services/pmlst
ENV PATH $PATH:/usr/src/cgepipeline/services/virulencefinder
ENV PATH $PATH:/usr/src/cgepipeline/services/plasmidfinder

# WORKDIR /usr/src/cgepipeline/services/resfinder
# # All perl modules share the same dependencies; only need to do it once
# RUN make install

# CGE framework
ENV INSTALLING_MODE DEV

# Install CGE service (Some services already included: KmerFinder...)
WORKDIR /usr/src/cgepipeline
RUN python setup.py install
# RUN make install

# Set convinience aliases
RUN echo "alias edit='emacs'" >> /.bashrc
RUN echo "alias ls='ls -h --color=tty'" >> /.bashrc
RUN echo "alias ll='ls -lrt'" >> /.bashrc
RUN echo "alias l='less'" >> /.bashrc
RUN echo "alias du='du -hP --max-depth=1'" >> /.bashrc
RUN echo "alias cwd='readlink -f .'" >> /.bashrc

# Set default working directory
WORKDIR /usr/src/cgepipeline/test
