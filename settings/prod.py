#!/usr/bin/env python
#

DEPENDENCIES_PROD = [
    # KmerFinder
    'git+https://git@bitbucket.org/genomicepidemiology/kmer-finder.git' +
    '@1.0.13#egg=KmerFinder-1.0.13',
]
