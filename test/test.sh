docker run -v ${1}:/input \
        -v ${2}:/output \
        cgetools VirulenceFinder-1.4.pl -d services/virulencefinder/database \
        -i /input/$3 \
        -s virulence_ecoli -k 95.00 \
        -o /output
