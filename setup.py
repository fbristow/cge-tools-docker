#!/usr/bin/env python

from setuptools import setup, find_packages
from settings import DEPENDENCIES
import os
import re

version = re.search(
    '__version__ = "([^\']+)"',
    open(
        os.path.join(os.path.dirname(__file__), 'settings/__init__.py')
    ).read()).group(1)

setup(
    name="modules",
    version=version,
    author="Your Name Here",
    author_email="youremail@example.com",
    url="https://bitbucket.org/genomicepidemiology/cge-functions-module/src",
    description="This project documents CGE Functions Module",
    long_description='\n\n'.join((
        open(os.path.join(os.path.dirname(__file__), 'README.md')).read(),
    )),
    entry_points={
        'console_scripts': [
            'ContigAnalyzer = pipelines.ContigAnalyzer:main',
            'KmerFinder = pipelines.KmerFinder:main',
            'ResFinder = pipelines.ResFinder:main',
            'MLST = pipelines.MLST:main',
            'PlasmidFinder = pipelines.PlasmidFinder:main',
            'pMLST = pipelines.pMLST:main',
            'VirulenceFinder = pipelines.VirulenceFinder:main',
            'BAP = pipelines.BAP:main'
            ]
        },
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
      'MySQL-python',
      'KmerFinder', # == 1.0.13
    ],
    dependency_links=DEPENDENCIES,
    test_suite="tests",
)
