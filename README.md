CGE Tools
===================

This project documents the Tools and the Pipeline of the Center for Genomic Epidemiology (CGE) running in a Docker container

Installation
=============

Recommended installation instructions can be found [here](https://docs.docker.com/engine/installation/) which install Docker and the VM.


To test that everything is installed:

```bash
docker version
docker run hello-world
```



Usage
=============

First we need to download the services from the BitBucket repository. Two options are available:

- SSH if your public key has been attached to your Bitbucket username (no password needed)
- HTTPS. You will have to enter your password for each repository.

If you have problems using SSH, follow this [link](https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html) to set it up

```bash
# Download CGE pipeline services from BitBucket. SSH is preferred when Public key authentication has been added to BitBucket
bash scripts/init.sh <USERNAME>
```
After this, the repository is ready to be installed as a Docker image.

```bash
# Additional commands

# Build repository as Docker image
docker build -t cgetools .

# Run terminal shell on selected image
docker run -t -i cgetools /bin/bash

# CGE services
# Assembler - SPADES-
docker run -v /absolute/path/to/input/folder:/input \
           -v /absolute/path/to/output/folder:/output \
           -ti --rm cgetools \
           spades.py --careful \
           -1 /input/YOUR_FILE_NAME \
           -2 /input/YOUR_FILE_NAME \
           --tmp-dir spades_tmp -m 5 -o /output
# KmerFinder
docker run -v /absolute/path/to/database/folder:/database \
           -v /absolute/path/to/input/folder:/input \
           -v /absolute/path/to/output/folder:/output \
           -ti --rm cgetools \
           findTemplate -i /input/YOUR_FILE_NAME \
           -t /database/DATABASE_FILE \
           -x "ATGAC" -w -o /output/out.txt
# MLST
docker run -v /absolute/path/to/input/folder:/input \
           -v /absolute/path/to/output/folder:/output \
           cgetools MLST-1.8.pl -d /usr/src/cgepipeline/services/mlst/database \
           -i /input/YOUR_FILE_NAME \
           -s ecoli \
           -o /output
# PlasmidFinder
docker run -v /absolute/path/to/input/folder:/input \
           -v /absolute/path/to/output/folder:/output \
           cgetools PlasmidFinder-1.3.pl -d /usr/src/cgepipeline/services/plasmidfinder/database \
           -i /input/YOUR_FILE_NAME \
           -p plasmid_database -k 95.00 \
           -o /output
# pMLST
docker run -v /absolute/path/to/input/folder:/input \
           -v /absolute/path/to/output/folder:/output \
           cgetools pMLST-1.4.pl -d /usr/src/cgepipeline/services/pmlst/database \
           -i /input/YOUR_FILE_NAME \
           -s incf \
           -o /output
# ResFinder
docker run -v /absolute/path/to/input/folder:/input \
           -v /absolute/path/to/output/folder:/output \
           cgetools ResFinder-2.1.pl -d /usr/src/cgepipeline/services/resfinder/database \
           -i /input/YOUR_FILE_NAME \
           -a aminoglycoside -k 95.00 -l 0.60 \
           -o /output
# VirulenceFinder
docker run -v /absolute/path/to/input/folder:/input \
           -v /absolute/path/to/output/folder:/output \
           cgetools VirulenceFinder-1.4.pl -d /usr/src/cgepipeline/services/virulencefinder/database \
           -i /input/YOUR_FILE_NAME \
           -s virulence_ecoli -k 95.00 \
           -o /output
```

As a test we provide a file in the test folder that runs one of the services:
```bash
# We assume that you named the repository cgetools and placed it in you home folder
# this example will not work if you downloaded somewhere else.
# cd into the test folder inside the repository
> user at MacBook-Pro in ~/cgetools/test
$ realpath .
~/cgetools/test # Copy the absolute path of the folder
> user at MacBook-Pro in ~/cgetools/test
# bash test.sh <input_folder> <output_folder> <input_file>
$ bash test.sh ~/cgetools/test ~/cgetools/test/output test.fsa
Done
# Check the output folder
```

Tests
=====
# Create and test KmerFinder DB example
```
printf "/databases/kmerfinder/database_fasta/GCF_000005845.2.fna\n/databases/kmerfinder/database_fasta/GCF_000006665.1.fna\n" > kfdb_file_list

maketemplatedb -l kfdb_file_list -o test.ATGAC -k 16 -x ATGAC

findTemplate -t test.ATGAC -i /usr/src/cgepipeline/test/pipeline_test.fa -o results.tab -k 16 -x ATGAC -w

head -2 results.tab
```

# Test BAP
```
docker run -ti --rm \
-v /your/path/to/cge-tools-docker/test/databases:/databases \
-v /your/path/to/cge-tools-docker/test:/workdir \
cgetools BAP --wdir /workdir --fa pipeline_test.fa

cat /your/path/to/cge-tools-docker/test/out.tsv
sequencing_size	genome_size	contigs	n50	depth	species	mlst	mlst_genes	resistance_genes	virulence_genes	plasmids	pmlsts
NA	4834953	606	28438	NA	Escherichia coli	ecoli[ST44],ecoli_2[ST2]	ecoli[adk-10,fumc-11,gyrb-4,icd-8,mdh-8,pura-8,reca-7],ecoli_2[dinb_8,icda_2,pabb_7,polb_3,putp_7,trpa_1,trpb_4,uida_2]	strA,aac(6')Ib-cr,aadA5,strB,aac(3)-IIa	gad	Col(MGD2),Col(MG828),ColRNAI,IncFII,IncFIB(AP001918),IncFIA	IncF[F31:A4:B1]
```

Useful commands
======================
```
# Go to Docker repository
cd /your/path/to/cge-tools-docker

# Update Docker repository
git stash;git pull

# Go to master git branch
git stash;git checkout master

# Shutdown Docker deamon
docker-machine stop default

# Start Docker deamon
docker-machine start default

# Reset Docker env (Used to run docker containers from different terminals)
eval "$(docker-machine env default)"

# Build cgetools Docker image
docker build -t cgetools .

# Docker Cleanup
# Stop and remove all containers (instances of images)
docker rm $(docker stop $(docker ps -aq))
# Remove all dangling images
docker rmi $(docker images -qf "dangling=true")
```

Documentation
=============

Extra commands to manage your images/containers
```bash
# Remove all containers
docker rm $(docker ps -a -q)

# Remove all images
docker rmi $(docker images -a -q)

# Remove exited containers
docker rm -v $(docker ps -a -q -f status=exited)

# Remove unwanted ‘dangling’ images.  
docker rmi $(docker images -f "dangling=true" -q)
```

License
=======

See LICENSE.md