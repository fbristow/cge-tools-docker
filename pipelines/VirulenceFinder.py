#!/usr/bin/env python
'''
################################################################################
##########                    CGE Service Wrapper                    ###########
################################################################################
'''
import sys, os, json

# INCLUDING THE CGE MODULES (No need to change this part)
#sys.path.append("/Users/mcft/Work/CGE/repositories/cge-tools-docker/modules/")
from cge import (check_file_type, debug, get_arguments, proglist, Program,
                 open_, adv_dict)

# SET GLOBAL VARIABLES
service, version = "VirulenceFinder", "1.4"
 
def main():
   ''' MAIN '''
   # PARSE ARGUMENTS
   # Add service specific arguments using the following format:
   #  (OPTION, VARIABLE,  DEFAULT,  HELP)
   args = get_arguments([
      ('-f', 'contigs', None, 'The input file (fasta file required)'),
      ('-k', 'threshold', '98.00', ('Identity threshold for reporting a BLAST hit.'
                                    )),
      # ('-l', 'minlength', '0.60', ('Minimum gene coveraage threshold for reporting'
      #                              ' a BLAST hit.')),
      ('-d', 'db_dir', '/databases/virulencefinder', 'Path to database directory'),
      ('-s',
       'databases',
       ('s.aureus_adherence,s.aureus_toxin,s.aureus_exoenzyme,s.aureus_hostimm,'
        's.aureus_secretion,virulence_ENT,virulence_ecoli'),
       'Comma-separated list of databases to BLAST the input sequences against.'
       ),
      ('-t', 'taxes', None, ('The lineage of the species in the sample '
                             'separated by semicolons'))
      ])
   
   # VALIDATE REQUIRED ARGUMENTS
   if args.db_dir is None:
      debug.graceful_exit("Input Error: No database directory was provided!\n")
   elif not os.path.exists(args.db_dir):
      debug.graceful_exit("Input Error: The specified database directory does not"
                          " exist!\n")
   else:
      # Check existence of config file
      db_config_file = '%s/config'%(args.db_dir)
      if not os.path.exists(db_config_file):
         debug.graceful_exit("Input Error: The database config file could not be "
                             "found!")
      # Check existence of notes file
      notes_path = "%s/notes.txt"%(args.db_dir)
      if not os.path.exists(notes_path):
         debug.log('Input Warning: notes.txt not found! (%s)'%(notes_path))
   
   if args.contigs is None:
      debug.graceful_exit("Input Error: No Contigs were provided!\n")
   elif not os.path.exists(args.contigs):
      debug.graceful_exit("Input Error: Contigs file does not exist!\n")
   elif check_file_type(args.contigs) != 'fasta':
      debug.graceful_exit(('Input Error: Invalid contigs format (%s)!\nOnly the '
                           'fasta format is recognised as a proper contig format.'
                           '\n')%(check_file_type(args.contigs)))
   else:
       args.contigs = os.path.abspath(args.contigs)
   
   if (args.databases is None or args.databases is '') and args.taxes is None:
      debug.graceful_exit("Input Error: Neither databases nor taxanomic lineage "
                          "were provided!\n")
   else:
      dbs = adv_dict({})
      extensions = []
      with open_(db_config_file) as f:
         for l in f:
            l = l.strip()
            if l == '': continue
            if l[0] == '#':
               if 'extensions:' in l:
                  extensions = [s.strip() for s in l.split('extensions:')[-1].split(',')]
               continue
            tmp = l.split('\t')
            if len(tmp) != 3:
               debug.graceful_exit(("Input Error: Invalid line in the database"
                                    " config file!\nA proper entry requires 3 tab "
                                    "separated columns!\n%s")%(l))
            db_prefix = tmp[0]
            name = tmp[1].split('#')[0]
            description = tmp[2]
            # Check if all db files are present
            for ext in extensions:
               db_path = "%s/%s.%s"%(args.db_dir, db_prefix, ext)
               if not os.path.exists(db_path):
                  debug.graceful_exit(("Input Error: The database file (%s) "
                                       "could not be found!")%(db_path))
            if name not in dbs: dbs[name] = []
            dbs[name].append(db_prefix)
      if len(dbs) == 0:
         debug.graceful_exit("Input Error: No databases were found in the "
                             "database config file!")
      if args.databases is not None:
         # Handle multiple databases
         args.databases = args.databases.split(',')
         # Check that the DBs are valid
         databases = []
         dbs = dbs.invert()
         for db_prefix in args.databases:
            if db_prefix in dbs:
               databases.append(db_prefix)
            else:
               debug.graceful_exit("Input Error: Provided database was not "
                                   "recognised! (%s)\n"%db_prefix)
      else:
         # Handle taxanomic input
         # Add manual DB exceptions
         dbs['Shigella'] = dbs['Escherichia coli']
         # Find all databases matching the provided lineage
         databases = []
         for tax in args.taxes.split(';'):
            tax = tax.strip()
            if tax == '': continue
            if tax in dbs:
               databases.extend(dbs[tax])
         if len(databases) == 0:
            sys.stdout.write("No databases fittet the provided taxonomy!")
            sys.exit(0)
   
   # Execute Program
   progname = 'VF'
   prog = Program(path='VirulenceFinder-1.4.pl',
      name=progname,
      args=['-d', args.db_dir,
            '-s', ','.join(databases),
            '-k', args.threshold,
            # '-l', args.minlength,
            '-i', args.contigs
            ]
      )
   prog.execute()
   proglist.add2list(prog)
   prog.wait(interval=10)
   
   # THE SUCCESS OF THE PROGRAMS ARE VALIDATED
   results = {
      'genes': []
   }
   status = prog.get_status()
   if status == 'Done' and os.path.exists('results_tab.txt'):
      with open('results_tab.txt', 'r') as f:
         for l in f:
            d=l.split('\t')
            if d[0] == 'Virulence factor': continue # skip header
            if len(d) > 5:
               # extract hit data
               gene_name, identity, dlen, alen, cname, pos, desc, db_name, acc = ['']*9
               try:
                  gene_name = d[0].strip()
                  identity =  float(d[1])/100
                  dlen, alen  = [int(x) for x in d[2].split('/')]
                  cname = d[3].strip()
                  pos = int(d[4].split('..')[0].strip())
                  desc = d[5].strip()
                  db_name = desc
                  acc = d[6].strip()
               except:
                  debug.print_out('Warning: Could not extract gene hit data! (%s)'%(d[0]))
                  db_name = d[5].strip()
               results['genes'].append(gene_name)
   
   if len(results['genes']) == 0:
      debug.graceful_exit('Error: No results were found!\n')
   else:
      # Print json to stdout
      sys.stdout.write('%s\n'%json.dumps(results))
   
   # LOG THE TIMERS
   proglist.print_timers() 

if __name__ == '__main__':
   main()
