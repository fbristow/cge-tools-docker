#!/usr/bin/env python
'''
################################################################################
##########                    CGE Service Wrapper                    ###########
################################################################################
'''
import sys, os, json

# INCLUDING THE CGE MODULES (No need to change this part)
#sys.path.append("/Users/mcft/Work/CGE/repositories/cge-tools-docker/modules/")
from cge import (check_file_type, debug, get_arguments, proglist, Program,
                 open_)

# SET GLOBAL VARIABLES
service, version = "KmerFinder", "2.1"

def main():
   ''' MAIN '''
   # PARSE ARGUMENTS
   # Add service specific arguments using the following format:
   #  (OPTION, VARIABLE,  DEFAULT,  HELP)
   args = get_arguments([
      ('-f', 'file', None, 'The input file (fasta file required)'),
      ('-w', 'winner', True, ('Use the winner takes it all approach?')),
      ('-d', 'db_dir', '/databases/kmerfinder', 'Path to database directory'),
      ('-s', 'databases', 'bacteria_organisms', (
         'Premade KmerFinder database name:'
         'bacteria_chromosomes, bacteria_organisms, plasmids, type_strains, '
         'virus_all, virus_humans, virus_vertebrates, fungi, protists, resfinder'
         )),
      ('-p', 'prefix', '', ('DB Prefix only used for private databases')),
      ])
   
   # VALIDATE REQUIRED ARGUMENTS
   if args.db_dir is None:
      debug.graceful_exit("Input Error: No database directory was provided!\n")
   elif not os.path.exists(args.db_dir):
      debug.graceful_exit("Input Error: The specified database directory does not"
                        " exist!\n")
   else:
      # Check existence of config file
      db_config_file = '%s/config'%(args.db_dir)
      if not os.path.exists(db_config_file):
         debug.graceful_exit("Input Error: The database config file could not be "
                           "found!")
   
   if args.file is None:
      debug.graceful_exit("Input Error: No input file were provided!\n")
   elif not os.path.exists(args.file):
      debug.graceful_exit("Input Error: Contigs file does not exist!\n")
   elif not check_file_type(args.file) in ['fasta','fastq']:
      debug.graceful_exit(('Input Error: Invalid file format (%s)!\nOnly the fasta'
                           ' or fastq format is recognised as a proper file '
                           'format.\n')%(check_file_type(args.file)))
   
   if (args.databases is None or args.databases is ''):
      debug.graceful_exit("Input Error: No database was specified!\n")
   else:
      dbs = {}
      extensions = []
      with open_(db_config_file) as f:
         for l in f:
            l = l.strip()
            if l == '': continue
            if l[0] == '#':
               if 'extensions:' in l:
                  extensions = [s.strip() for s in l.split('extensions:')[-1].split(',')]
               continue
            tmp = l.split('\t')
            if len(tmp) != 3:
               debug.graceful_exit(("Input Error: Invalid line in the database"
                                    " config file!\nA proper entry requires 3 tab "
                                    "separated columns!\n%s")%(l))
            db_prefix = tmp[0]
            name = tmp[1].lower().replace(' ', '_')
            description = tmp[2]
            # Check if all db files are present
            for ext in extensions:
               db_path = "%s/%s.%s"%(args.db_dir, db_prefix, ext)
               if not os.path.exists(db_path):
                  debug.graceful_exit(("Input Error: The database file (%s) "
                                       "could not be found!")%(db_path))
            # Check existence of taxonomy file
            tax_path = "%s/%s.tax"%(args.db_dir, db_prefix.split('.')[0])
            if not os.path.exists(tax_path):
               debug.log('Warning: Taxonomy file not found! (%s)'%(tax_path))
               tax_path = None
            kmer_prefix = ''
            if 'prefix=' in description:
               kmer_prefix = description.split('prefix=')[-1].strip()
            if name not in dbs: dbs[name] = []
            dbs[name].append((db_prefix, tax_path, kmer_prefix))
      
      if len(dbs) == 0:
         debug.graceful_exit("Input Error: No databases were found in the "
                           "database config file!")
      # Handle multiple databases
      args.databases = args.databases.split(',')
      # Check that the ResFinder DBs are valid
      databases = []
      for db in args.databases:
         if db.lower() in dbs:
            # Predefined DB
            db = db.lower()
            databases.append((db, dbs[db][0][0], dbs[db][0][1], dbs[db][0][2]))
         elif os.path.exists(db):
            # Private DB
            databases.append(('private', db, None, args.prefix))
         else:
            debug.graceful_exit("Input Error: Provided database was not recognised"
                              " or found! (%s)\n"%db)
   
   # Execute scripts
   for (db_name, db_prefix, tax_path, prefix) in databases:
      progname = 'TF_%s'%(db_name)
      tf_result = 'results.tab'
      prog = Program(path='findTemplate',
         name=progname,
         args=['-t', "%s/%s"%(args.db_dir, db_prefix),
               '-i', args.file,
               '-o', tf_result
               ]
         )
      # Add conditional arguments
      if len(prefix) > 0:
         prog.append_args(['-x', prefix])
      if args.winner:
         prog.append_args(['-w'])
      
      # Execute and wait on species prediction
      prog.execute()
      proglist.add2list(prog)
      prog.wait(interval=25)
      
      # Run conditional program
      if tax_path is not None:
         # Get taxonomy annotation
         progname = 'tax'
         tax_result = 'results_tax.tab'
         prog = Program(path='getTax',
            name=progname,
            args=['-i', tf_result,
                  '-t', tax_path,
                  '-o', tax_result
                  ]
            )
         # Add conditional arguments
         tax_core = tax_path.split('/')[-1].split('.')[0]
         if tax_core in ["bacteria", "fungi", "protists", "viruses", "plasmids",
                         "test"]:
            prog.append_args(['-b'])
         if tax_core in ["bacteria", "fungi", "protists", "viruses"]:
            prog.append_args(['-c'])
         # Execute and wait on taxonomy annotation
         proglist.add2list(prog)
         prog.execute()
         prog.wait(interval=2)
         
         # # Get Final Output (HTML link)
         # progname = 'fin'
         # fin_result = 'results_final.tab'
         # prog = Program(path='createTable',
         #    name=progname,
         #    args=['-i', tax_result,
         #          '-d', args.database,
         #          '-o', fin_result
         #          ]
         #    )
         # # Execute and wait on taxonomy annotation
         # proglist.add2list(prog)
         # prog.execute()
         # prog.wait(interval=2)
      else:
         tax_result = tf_result
      
      # THE SUCCESS OF THE PROGRAMS IS VALIDATED
      lineage = None
      predicted_species = None
      taxid_species = None
      closest_match = None
      taxid_match = None
      Kmer_hits = None
      Kmers_template = None
      Kmer_coverage = None
      coverage_query = None
      coverage_match = None
      status = proglist['TF_%s'%(db_name)].get_status()
      if status == 'Done':
         try:
            debug.log('KmerFinder result_file: %s'%tax_result)
            with open(tax_result) as f:
               # Skip header
               _ = f.readline()
               hit = f.readline().split('\t') # First hit
               # Kmer_hits = int(hit[1].strip())
               predicted_species = hit[-1].strip() if float(hit[6].strip())/100.0 > 0.5 else 'unknown'
               # taxid_species = hit[-2].strip()
               lineage = [x.strip() for x in hit[-3].split(';') if x.strip() != '']
               # closest_match = lineage[-1].strip()
               # taxid_match = hit[-4].strip()
               # coverage_query = float(hit[5].strip())/100
               # coverage_match = float(hit[6].strip())/100
               # Kmer_coverage = float(hit[7].strip())
               # Kmers_template = float(hit[8].strip())
         except Exception, e:
            debug.log('All or some Kmer Results could not be found!\n%s'%(e))
         else: status = 'Success'
      else: status = 'Failure'
      
      if status not in ['Done','Success']:
         debug.graceful_exit("Error: Execution of the program failed!\n")
   
   # Sumarise Results
   results = {
      'lineage':         lineage,
      'species':         predicted_species
      # 'taxid_species':   taxid_species,
      # 'closest_match':   closest_match,
      # 'taxid_match':     taxid_match,
      # 'kmers_hit':       Kmer_hits,
      # 'kmers_template':  Kmers_template,
      # 'kmer_coverage':   Kmer_coverage,
      # 'coverage_query':  coverage_query,
      # 'coverage_match':  coverage_match,
   }
   debug.log('KmerFinder results: %s'%results)
   
   if results['lineage'] is None:
      debug.graceful_exit('Error: No results were found!\n')
   else:
      sys.stdout.write('%s\n'%json.dumps(results))
   
   # LOG THE TIMERS
   proglist.print_timers() 

if __name__ == '__main__':
   main()