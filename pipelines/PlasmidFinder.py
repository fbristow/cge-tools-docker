#!/usr/bin/env python
'''
################################################################################
##########                    CGE Service Wrapper                    ###########
################################################################################
'''
import sys, os, json

# INCLUDING THE CGE MODULES (No need to change this part)
# sys.path.insert(1, "/tools/")
from cge import (check_file_type, debug, get_arguments, proglist, Program,
                 mkpath, open_, adv_dict)

# SET GLOBAL VARIABLES
service, version = "PlasmidFinder", "1.3"

def main():
   ''' MAIN '''
   # PARSE ARGUMENTS
   # Add service specific arguments using the following format:
   #  (OPTION, VARIABLE,  DEFAULT,  HELP)
   args = get_arguments([
      ('-f', 'contigs', None, 'The input file (fasta file required)'),
      ('-k', 'threshold', '80.00', ('Identity threshold for reporting a BLAST hit.'
                                    )),
      # ('-l', 'minlength', '0.60', ('Minimum gene coveraage threshold for reporting'
      #                              ' a BLAST hit.')),
      ('-d', 'db_dir', '/databases/plasmidfinder', 'Path to database directory'),
      ('-s', 'databases', 'plasmid_database', ('Comma-separated list of databases '
                                               'to BLAST the input sequences '
                                               'against.'))   # ,plasmid_positiv
      ])
   
   # VALIDATE REQUIRED ARGUMENTS
   if args.db_dir is None:
      debug.graceful_exit("Input Error: No database directory was provided!\n")
   elif not os.path.exists(args.db_dir):
      debug.graceful_exit("Input Error: The specified database directory does not"
                          " exist!\n")
   else:
      # Check existence of config file
      db_config_file = '%s/config'%(args.db_dir)
      if not os.path.exists(db_config_file):
         debug.graceful_exit("Input Error: The database config file could not be "
                             "found!")
   
   if args.contigs is None:
      debug.graceful_exit("Input Error: No Contigs were provided!\n")
   elif not os.path.exists(args.contigs):
      debug.graceful_exit("Input Error: Contigs file does not exist!\n")
   elif check_file_type(args.contigs) != 'fasta':
      debug.graceful_exit(('Input Error: Invalid contigs format (%s)!\nOnly the '
                           'fasta format is recognised as a proper contig format.'
                           '\n')%(check_file_type(args.contigs)))
   else:
       args.contigs = os.path.abspath(args.contigs)
   
   if (args.databases is None or args.databases is ''):
      debug.graceful_exit("Input Error: No database was specified!\n")
   else:
      dbs = adv_dict({})
      extensions = []
      with open_(db_config_file) as f:
         for l in f:
            l = l.strip()
            if l == '': continue
            if l[0] == '#':
               if 'extensions:' in l:
                  extensions = [s.strip() for s in l.split('extensions:')[-1].split(',')]
               continue
            tmp = l.split('\t')
            if len(tmp) != 3:
               debug.graceful_exit(("Input Error: Invalid line in the database"
                                    " config file!\nA proper entry requires 3 tab "
                                    "separated columns!\n%s")%(l))
            db_prefix = tmp[0]
            name = tmp[1].split('#')[0]
            description = tmp[2]
            # Check if all db files are present
            for ext in extensions:
               db_path = "%s/%s.%s"%(args.db_dir, db_prefix, ext)
               if not os.path.exists(db_path):
                   debug.graceful_exit(("Input Error: The database file (%s) "
                                        "could not be found!")%(db_path))
            if name not in dbs: dbs[name] = []
            dbs[name].append(db_prefix)
      if len(dbs) == 0:
         debug.graceful_exit("Input Error: No databases were found in the "
                             "database config file!")
      dbs = dbs.invert()
      # Handle multiple databases
      args.databases = args.databases.split(',')
      # Check that the ResFinder DBs are valid
      databases = []
      for db_prefix in args.databases:
         if db_prefix in dbs:
            databases.append(db_prefix)
         else:
            debug.graceful_exit("Input Error: Provided database was not "
                                "recognised! (%s)\n"%db_prefix)
   
   # Execute Programs
   for db_prefix in databases:
      progname = 'PF_%s'%db_prefix
      prog = Program(path='PlasmidFinder-1.3.pl',
         name=progname, wdir=progname,
         args=['-d', args.db_dir,
               '-p', db_prefix,
               '-k', args.threshold,
               # '-l', args.minlength,
               '-i', args.contigs
               ]
         )
      prog.execute()
      proglist.add2list(prog)
   
   # SUMARISE RESULTS
   results = {
      'plasmids':    [],
      'pmlsts':      []
      }
   
   for progname in proglist.list:
      if not 'PF' in progname: continue # Skip all but PlasmidFinder
      prog = proglist[progname]
      prog.wait(interval=10)
      # GET RESULTS
      status = prog.get_status()
      if status == 'Done':
         # FIND PLASMIDS AND CHECK FOR KNOWN PMLST SCHEMES SUBJECTS
         schemes = adv_dict({
            'IncF':     ['FIA', 'FIB', 'FII', 'FIC'],
            'IncHI1':   ['HI1A', 'HI1B'],
            'IncHI2':   ['HI2', 'HI2A'],
            'IncI1':    ['I1'],
            'IncN':     ['N', 'N2', 'N3']
         })
         schemes_inv = schemes.invert()
         plasmids = {}
         pmlsts = {}
         with open('%s/results_tab.txt'%(progname), 'r') as f:
            #f = '''
            #Plasmid	Identity	Query/HSP	Contig	Position in contig	Note	Accession no.
            #FIB(AP001918)	98.39	682/682	NODE_192_length_3637_cov_14.801209	1877..2558		AP001918
            #FII	95.67	261/254	NODE_474_length_521_cov_31.654510	312..565		AY458016
            #FIA	99.74	388/384	NODE_102_length_9773_cov_14.911490	3387..3770		AP001918
            #HI1A(CIT)	99.74	388/384	NODE_102_length_9773_cov_14.911490	3387..3770		AP001918
            #Col156	99.74	388/384	NODE_102_length_9773_cov_14.911490	3387..3770		AP001918
            #'''
            for l in f:
               d=l.strip().split()
               if len(d) > 3:
                  if d[0] == 'Plasmid': continue # skip header
                  # compute quality ID * overlap fraction
                  identity =  float(d[1])/100
                  overlap  =  [int(x) for x in d[2].split('/')]
                  quality  =  round( identity * overlap[0] / overlap[1], 4 )
                  # Skip already seen plasmid with worse BLAST score
                  if d[0] in plasmids and quality < plasmids[d[0]]['quality']:
                     continue
                  else:
                     # Save plasmid record
                     plasmids[d[0]] = {
                        'plasmid':   d[0],
                        'identity':  identity,
                        'overlap':   overlap,
                        'quality':   quality
                     }
                     # Check if there exists a pMLST scheme for the plasmid
                     pid = d[0].split('(')[0].replace('Inc','')
                     scheme = schemes_inv.get(pid, [None])[0]
                     if not scheme is None:
                        pmlsts[scheme] = []
         results['plasmids'].extend(plasmids.values())
         
         # Execute PMLST
         for scheme in sorted(pmlsts):
            debug.log("Run pMLST %s\n"%scheme)
            progname='pMLST_'+scheme
            mkpath(progname) # Create service specific directory
            prog = Program(path='pMLST',
                           name=progname, wdir=progname,
                           args=['-f', args.contigs,
                                 '-s', scheme.lower(),
                                 ]
                           )
            prog.execute()
            proglist.add2list(prog)
         
         # WAIT ON PMLST SERVICES TO FINISH
         debug.log('Extracting pMLST scheme results...')
         for progname in proglist.list:
            if not 'pMLST' in progname: continue # Skip all but pMLST
            prog = proglist[progname]
            prog.wait(interval=15)
            scheme = progname.split('_')[1]
            if isinstance(prog.stdout, str):
               fp = prog.stdout
               if prog.wdir != '':
                  fp = '%s/%s'%(prog.wdir, fp)
               with open(fp) as f:
                  debug.log('Getting results from %s'%f)
                  try:
                     pmlst_results = json.load(f)
                  except ValueError, e:
                     pmlst_results = None
            else:
               try:
                  pmlst_results = json.load(prog.stdout)
               except ValueError, e:
                  pmlst_results = None
            if pmlst_results is not None:
               pmlsts[scheme].append({
                  'scheme_name':    scheme,
                  'sequence_type':  pmlst_results['sequence_type'],
                  'closest_match':  pmlst_results['closest_match']
                  })
            else:
               debug.log('Warning: No json result was found!')
            results['pmlsts'].extend(pmlsts[scheme])
   
   if len(results['plasmids']) == 0:
      debug.graceful_exit('Error: No results were found!\n')
   else:
      # Print json to stdout
      sys.stdout.write('%s\n'%json.dumps(results))
   
   # LOG THE TIMERS
   proglist.print_timers() 

if __name__ == '__main__':
   main()
