#!/usr/bin/env python
'''
################################################################################
##########                    CGE Service Wrapper                    ###########
################################################################################
'''
import sys, os, json

# INCLUDING THE CGE MODULES (No need to change this part)
#sys.path.append("/Users/mcft/Work/CGE/repositories/cge-tools-docker/modules/")
from cge import check_file_type, debug, get_arguments, proglist, Program, open_, adv_dict

# SET GLOBAL VARIABLES
service, version = "PlasmidFinder", "1.3"

def main():
   ''' MAIN '''
   # PARSE ARGUMENTS
   # Add service specific arguments using the following format:
   #  (OPTION, VARIABLE,  DEFAULT,  HELP)
   args = get_arguments([
      ('-f', 'contigs', None, 'The input file (fasta file required)'),
      # ('-k', 'threshold', '80.00', ('Identity threshold for reporting a BLAST hit.'
      #                               )),
      # ('-l', 'minlength', '0.60', ('Minimum gene coveraage threshold for reporting'
      #                              ' a BLAST hit.')),
      ('-d', 'db_dir', '/databases/pmlst', 'Path to database directory'),
      ('-s', 'database', None, 'database to BLAST the input sequences against.')
      ])
   
   # VALIDATE REQUIRED ARGUMENTS
   if args.db_dir is None:
      debug.graceful_exit("Input Error: No database directory was provided!\n")
   elif not os.path.exists(args.db_dir):
      debug.graceful_exit("Input Error: The specified database directory does not"
                          " exist!\n")
   else:
      # Check existence of config file
      db_config_file = '%s/config'%(args.db_dir)
      if not os.path.exists(db_config_file):
         debug.graceful_exit("Input Error: The database config file could not be "
                             "found!")
   
   if args.contigs is None:
      debug.graceful_exit("Input Error: No Contigs were provided!\n")
   elif not os.path.exists(args.contigs):
      debug.graceful_exit("Input Error: Contigs file does not exist!\n")
   elif check_file_type(args.contigs) != 'fasta':
      debug.graceful_exit(('Input Error: Invalid contigs format (%s)!\nOnly the '
                           'fasta format is recognised as a proper contig format.'
                           '\n')%(check_file_type(args.contigs)))
   else:
       args.contigs = os.path.abspath(args.contigs)
   
   if (args.database is None or args.database is ''):
      debug.graceful_exit("Input Error: No database was specified!\n")
   else:
      dbs = adv_dict({})
      extensions = []
      with open_(db_config_file) as f:
         for l in f:
            l = l.strip()
            if l == '': continue
            if l[0] == '#':
               if 'extensions:' in l:
                  extensions = [s.strip() for s in l.split('extensions:')[-1].split(',')]
               continue
            tmp = l.split('\t')
            if len(tmp) != 3:
               debug.graceful_exit(("Input Error: Invalid line in the database"
                                    " config file!\nA proper entry requires 3 tab "
                                    "separated columns!\n%s")%(l))
            db_prefix = tmp[0]
            name = tmp[1].split('#')[0]
            description = tmp[2]
            # Check if all db files are present
            for ext in extensions:
               db_path = "%s/%s.%s"%(args.db_dir, db_prefix, ext)
               if not os.path.exists(db_path):
                  debug.graceful_exit(("Input Error: The database file (%s) "
                                       "could not be found!")%(db_path))
            if name not in dbs: dbs[name] = []
            dbs[name].append(db_prefix)
      if len(dbs) == 0:
         debug.graceful_exit("Input Error: No databases were found in the "
                             "database config file!")
      dbs = dbs.invert()
      # Handle multiple databases
      args.database = args.database.split(',')
      # Check that the ResFinder DBs are valid
      databases = []
      for db_prefix in args.database:
         if db_prefix in dbs:
            databases.append(db_prefix)
         else:
            debug.graceful_exit("Input Error: Provided database was not "
                                "recognised! (%s)\n"%db_prefix)
   
   # Execute Programs
   progname = 'pMLST'
   prog = Program(path='pMLST-1.4.pl',
       name=progname,
       args=['-d', args.db_dir,
           '-s', ','.join(databases),
           # '-k', args.threshold,
           # '-l', args.minlength,
           '-i', args.contigs#,
           # '-c'
           ]
       )
   prog.execute()
   proglist.add2list(prog)
   
   prog.wait(interval=10)
   
   # GET RESULTS
   results = {
      'scheme_name':    None,
      'closest_match':  None,
      'sequence_type':  None
      }
   status = prog.get_status()
   if status == 'Done':
      with open('results.txt', 'r') as f:
         for l in f:
            d = [ld.strip() for ld in l.split(':',1)]
            if d[0] == 'Scheme':
               results['scheme_name'] = d[1]
            if d[0] == 'Sequence Type':
               results['sequence_type'] = d[1].strip(' ][')
            if d[0] == 'Closest match':
               results['closest_match'] = d[1].strip(' ][')
   
   if results['sequence_type'] is None:
      debug.graceful_exit('Error: No results were found!\n')
   else:
      # Print json to stdout
      sys.stdout.write('%s\n'%json.dumps(results))
   
   # LOG THE TIMERS
   proglist.print_timers() 

if __name__ == '__main__':
   main()
