#!/usr/bin/env python
'''
################################################################################
##########                    CGE Service Wrapper                    ###########
################################################################################
'''
import sys, os, json

# INCLUDING THE CGE MODULES (No need to change this part)
#sys.path.append("/Users/mcft/Work/CGE/repositories/cge-tools-docker/modules/")
from cge import (check_file_type, debug, get_arguments, proglist, Program,
                 adv_dict, open_, mkpath)

# SET GLOBAL VARIABLES
service, version = "MLST", "1.8"
   
def main():
   ''' MAIN '''
   # PARSE ARGUMENTS
   # Add service specific arguments using the following format:
   #  (OPTION, VARIABLE,  DEFAULT,  HELP)
   args = get_arguments([
      ('-f', 'contigs', None, 'The input file (fasta file required)'),
      ('-o', 'organism', None, ('Scientific name of the organism in the sample '
                                 '(used to predict the MLST scheme)')),
      ('-d', 'db_dir', '/databases/mlst', 'Path to database directory'),
      ('-s', 'mlst_scheme', None, 'The MLST scheme')
      ])
   
   # VALIDATE REQUIRED ARGUMENTS
   if args.db_dir is None:
      debug.graceful_exit("Input Error: No database directory was provided!\n")
   elif not os.path.exists(args.db_dir):
      debug.graceful_exit("Input Error: The specified database directory does not"
                          " exist!\n")
   else:
      # Check existence of config file
      db_config_file = '%s/config'%(args.db_dir)
      if not os.path.exists(db_config_file):
         debug.graceful_exit("Input Error: The database config file could not be "
                             "found!")
   
   if args.contigs is None:
      debug.graceful_exit("Input Error: No Contigs were provided!\n")
   elif not os.path.exists(args.contigs):
      debug.graceful_exit("Input Error: Contigs file does not exist!\n")
   elif check_file_type(args.contigs) != 'fasta':
      debug.graceful_exit(('Input Error: Invalid contigs format (%s)!\nOnly the '
                           'fasta format is recognised as a proper contig format.'
                           '\n')%(check_file_type(args.contigs)))
   else:
       args.contigs = os.path.abspath(args.contigs)
   
   if args.mlst_scheme is None and args.organism is None:
      debug.graceful_exit("Input Error: Neither MLST scheme nor organism were "
                          "provided!\n")
   else:
      dbs = adv_dict({})
      extensions = []
      with open_(db_config_file) as f:
         for l in f:
            l = l.strip()
            if l == '': continue
            if l[0] == '#':
               if 'extensions:' in l:
                  extensions = [s.strip() for s in l.split('extensions:')[-1].split(',')]
               continue
            tmp = l.split('\t')
            if len(tmp) != 3:
               debug.graceful_exit(("Input Error: Invalid line in the database"
                                    " config file!\nA proper entry requires 3 tab "
                                    "separated columns!\n%s")%(l))
            db_prefix = tmp[0]
            name = tmp[1].split('#')[0]
            description = tmp[2]
            # Check if all db files are present
            for ext in extensions:
               db_path = "%s/%s.%s"%(args.db_dir, db_prefix, ext)
               if not os.path.exists(db_path):
                   debug.graceful_exit(("Input Error: The database file (%s) "
                                        "could not be found!")%(db_path))
            dbs[db_prefix] = name
      if len(dbs) == 0:
         debug.graceful_exit("Input Error: No databases were found in the "
                             "database config file!")
      if args.mlst_scheme is not None:
         # Handle multiple MLST schemes
         args.mlst_scheme = args.mlst_scheme.split(',')
         # Check that the MLST scheme is valid
         databases = []
         for db_prefix in args.mlst_scheme:
            if db_prefix in dbs:
               databases.append(db_prefix)
            else:
               debug.graceful_exit("Input Error: Provided MLST scheme was not "
                                   "recognised! (%s)\n"%db_prefix)
      else:
         # Predict MLST scheme from args.organism input
         # Flip MLST scheme dict
         dbs = dbs.invert()
         # Add Manual Species Exceptions
         dbs['Shigella spp.'] = dbs['Escherichia coli']
         # Predict MLST Scheme from organism
         genus_lvl_name = args.organism.split()[0] + ' spp.'
         databases = dbs.get(args.organism, dbs.get(genus_lvl_name, None))
         if databases is None:
            debug.graceful_exit(
               ('No appropriate MLST scheme could be found for the provided '
                'organism! Following schemes were tried: %s and %s'
                )%(args.organism, genus_lvl_name))
   
   # Execute MLST for each scheme
   for db_prefix in databases:
      progname = 'MLST_%s'%db_prefix
      mkpath(progname) # Create service specific directory
      prog = Program(path='MLST-1.8.pl',
         name=progname, wdir=progname,
         args=['-d', args.db_dir,
               '-s', db_prefix,
               '-i', args.contigs
               ]
         )
      prog.execute()
      proglist.add2list(prog)
   
   # Wait for and extract results
   results = {
      'runs': []
   }
   for progname in proglist.list:
      prog = proglist[progname]
      prog.wait(interval=30)
      # THE SUCCESS OF THE PROGRAMS ARE VALIDATED
      status = prog.get_status()
      if status == 'Done' and os.path.exists('%s/results.txt'%progname):
         result = {
            'scheme_name':   '',
            'sequence_type': '',
            'genes':         []
            }
         with open('%s/results.txt'%progname, 'r') as f:
            for l in f:
               d = [ld.strip() for ld in l.split(':')]
               if d[0] == 'MLST Profile':
                  result['scheme_name'] = d[1]
               elif d[0] == 'Sequence Type':
                  result['sequence_type'] = d[1]
         with open('%s/results_tab.txt'%progname, 'r') as f:
            for l in f:
               d=l.split('\t')
               if d[0] == 'Gene': continue # skip header
               if len(d) == 6:
                  # extract hit data
                  locus, identity, hlen, alen, gaps, best = ['']*6
                  try:
                     locus = d[0].strip()
                     identity =  float(d[1])/100
                     hlen = int(d[2])
                     alen = int(d[3])
                     gaps = int(d[4].strip())
                     best = d[5].strip()
                  except:
                     debug.print_out('Warning: Could not extract gene hit data! (%s)'%(d[0]))
                  result['genes'].append(best)
         results['runs'].append(result)
   
   if len(results['runs']) == 0:
      debug.graceful_exit('Error: No results were found!\n')
   else:
      # Print json to stdout
      sys.stdout.write('%s\n'%json.dumps(results))
   
   # LOG THE TIMERS
   proglist.print_timers() 

if __name__ == '__main__':
   main()
