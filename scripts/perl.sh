#!/bin/env bash
#
curl -L https://cpanmin.us -k | perl - App::cpanminus

cpan App::perlbrew
cpanm Try::Tiny::Retry

perlbrew init

# Always exit with ok status
exit 0
