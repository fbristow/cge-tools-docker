#!/bin/env bash
#
echo "Args: " ${1}

if [ -z "${1}" ]; then
   echo "User name to repository missing!"
   exit
else
    USERNAME=${1}
fi

mkdir services

echo "Choose your credential to the repo [ssh]/[https]"
read answer
if  [ $answer == 'ssh' ]; then
    echo "Using public key authentication";
    git clone git@bitbucket.org:genomicepidemiology/resfinder.git services/resfinder
    git clone git@bitbucket.org:genomicepidemiology/plasmidfinder.git services/plasmidfinder
    git clone git@bitbucket.org:genomicepidemiology/mlst.git services/mlst
    git clone git@bitbucket.org:genomicepidemiology/pmlst.git services/pmlst
    git clone git@bitbucket.org:genomicepidemiology/virulencefinder.git services/virulencefinder
elif  [ $answer == 'https' ]; then
    echo "Password should be entered for each repository";
    git clone "https://${USERNAME}@bitbucket.org/genomicepidemiology/resfinder.git" services/resfinder
    git clone "https://${USERNAME}@bitbucket.org/genomicepidemiology/plasmidfinder.git" services/plasmidfinder
    git clone "https://${USERNAME}@bitbucket.org/genomicepidemiology/mlst.git" services/mlst
    git clone "https://${USERNAME}@bitbucket.org/genomicepidemiology/pmlst.git" services/pmlst
    git clone "https://${USERNAME}@bitbucket.org/genomicepidemiology/virulencefinder.git" services/virulencefinder

else
    echo "Only ssh/https are valid options";
    exit
fi


# Allow scripts to be executed
find services -type f -iname "*.pl" -print -exec chmod 665 {} \;

# Always exit with ok status
exit 0
