#!/usr/bin/env bash
#
echo 'Welcome to the CGE tools Docker container'
echo '_______________'
echo 'Usage:'
echo '---------------'
echo 'plasmidfinder'
echo 'resfinder'
echo 'mlst'
echo 'pmlst'
echo 'virulencefinder'

# Always exit with ok status
exit 0
